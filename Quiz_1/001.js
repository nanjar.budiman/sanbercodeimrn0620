/*******************
 *  A. Bandingkan Angka (10 poin)
 */

 function bandingkan(param1, param2){
     var hasil ;
    if ( (param1 < 0 ) || (param2 < 0) ){
        return -1;
    }else{
        if (parseInt(param1) < parseInt(param2)){
            hasil = "Parameter 2 Lebih besar dari Parameter 1";
            return hasil;
        }else if (parseInt(param1) > parseInt(param2)){
            hasil = "Parameter 1 Lebih besar dari Parameter 2";
            return hasil;
        }else{
            return -1;
        }
    }
 }

 console.log(bandingkan(1,3));
console.log('---------------------');
 console.log('');
 /*******************
 *  B. Balik String (10 poin)
 */

 function balikString(kata){
    
    var tmp = '';
    var panjang = kata.length;
    while ( panjang > 0){
        panjang--;
        tmp += kata[panjang] ;
        
    }
    return tmp;
}
console.log(balikString("Javascript"));

console.log('---------------------');
console.log('');

/*******************
 *  C. Palindrome (10 poin)
 */

function palindrome(strInput) {
     var tmpStr = balikString(strInput);


     if (tmpStr == strInput){
         return true;
     }else{
         return false;
     }

}

console.log(palindrome('jakarta'));
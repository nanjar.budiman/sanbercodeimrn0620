/*******************
 *  A. Descending Ten (10 poin)
 */
console.log('A. Descending Ten (10 poin)');
function DescendingTen(num) {
    var i,j ;
    i = 10;
    j=1;
    var strNum ;
    var arrNum = [];
    tmp = num;
    if (arguments.length == 0){
        return -1;
    }else{
        while(j <= i){
            //strNum = String(tmp) + strNum  ;
            //console.log(num);
            arrNum.push(tmp);
            tmp = parseInt(num) - parseInt(j);
            
          // console.log ('Tmp : '+ tmp +' j: '+ j);
           j++;
        }
        return arrNum.join(' ');
    }
    
}

console.log(DescendingTen(20));
console.log('---------------------');
console.log('');

/*******************
 *  B. Ascending Ten (10 poin)
 */
console.log('B. Ascending Ten (10 poin)');
function AscendingTen(num) {
    var i,j ;
    
    j=1;
    var strNum ;
    var arrNum = [];
    tmp = num;
    if (arguments.length == 0){
        return -1;
    }else{
        while(j <= 10){
            
            arrNum.push(tmp);
            tmp = parseInt(num) + parseInt(j);
            
          // console.log ('Tmp : '+ tmp +' j: '+ j);
           j++;
        }
        return arrNum.join(' ');
    }
}

console.log(AscendingTen(10));

console.log('---------------------');
console.log('');
/*******************
 *   C. Conditional Ascending Descending (15)
 */
console.log('C. Conditional Ascending Descending (15)');

function ConditionalAscDesc(reference, check) {
    var ganjil;
    if (arguments.length < 2){
        return -1;
    }else{
        if ( (check % 2 == 1) ){
            ganjil = true;
        }else{
            ganjil = false;
        }
    
        if (ganjil){
            return AscendingTen(reference);
        }else{
            return DescendingTen(reference);
        }
    }
    
}

console.log(ConditionalAscDesc(20, 8));

console.log('---------------------');
console.log('');
/*******************
 *   C. Conditional Ascending Descending (15)
 */

function ularTangga() {
   var startPoint = 100;   
    var tmpstr = '';
    for (var row=1;row<=10;row++){    
        if ( (row % 2 == 1) ){
            console.log(DescendingTen(startPoint));
            startPoint -= 11;
        }else{
            console.log(AscendingTen(startPoint));
            startPoint -= 11;
        }
        
    }
}

ularTangga() ;
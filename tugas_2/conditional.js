//variable input
console.log('Tugas Conditional If-else');
console.log('---------------------------------------');
console.log('');
var nama = "John";
var peran = "Penyihir";

if (nama != ''){
    if (peran != ''){
        console.log('Selamat datang di Dunia Werewolf, ' + nama);
        if (peran == 'Penyihir'){
            console.log('Halo Penyihir '+nama+', kamu dapat melihat siapa yang menjadi werewolf!');
        }else if (peran == 'Guard'){
            console.log('Halo Guard '+nama+', kamu akan membantu melindungi temanmu dari serangan werewolf!');
        }else if (peran == 'Werewolf'){
            console.log('Halo Werewolf '+nama+', Kamu akan memakan mangsa setiap malam!');
        }
    }else{
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!')
    }
}else{
    console.log('Nama harus diisi!');
}

console.log('');
console.log('---------------------------------------');
console.log('');

console.log('Tugas Conditional Switch Case');
console.log('---------------------------------------');
console.log('');

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var strBulan = "";
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

switch (bulan){
    case 1 : {
                strBulan = "Januari";
                break;
            }
    case 2 : {
                strBulan = "Februari";
                break;
            }
    case 3 : {
                strBulan = "Maret";
                break;
            }
    case 4 : {
                strBulan = "April";
                break;
            }
    case 5 : {
                strBulan = "Mei";
                break;
            }
    case 6 : {
                strBulan = "Juni";
                break;
            }

    case 7 : {
                strBulan = "Juli";
                break;
            }
    case 8 : {
                strBulan = "Agustus";
                break;
            }
    case 9 : {
                strBulan = "September";
                break;
            }
    case 10 : {
                strBulan = "Oktober";
                break;
            }
    case 11 : {
                strBulan = "November";
                break;
            }
    case 12 : {
                strBulan = "Desember";
                break;
            }
}

console.log(hari + ' ' + strBulan + ' ' + tahun);
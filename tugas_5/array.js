console.log('Soal No. 1 (Range)');
console.log('------------------');

function range(startNum, finishNum){
    
    var hasil = [];
    var i;
    
    if (arguments.length != 2){
        hasil.push(-1);
        return hasil;
    }else{
        if (startNum < finishNum){
            for (i=startNum; i<= finishNum;i++){
                hasil.push(i);
            }
        }else if(startNum > finishNum){            
            for (i=finishNum; i<= startNum;i++){               
                hasil.unshift(i);                
            }            
        }
        return hasil;
    }           
}

console.log(range(1,10));
console.log('------------------');
console.log('');

console.log('Soal No. 2 (Range with Step)');
console.log('------------------');

function rangeWithStep(startNum, finishNum, step){
    
    var hasil = [];
    var i, tmp = 0;
    var counter ;
    
    if (arguments.length != 3){
        hasil.push(-1);
        return hasil;
    }else{
        if (startNum < finishNum){
            while (startNum <= finishNum) {
                hasil.push(startNum);
                startNum += step ;
            }
             
        }else if(startNum > finishNum){     
            counter = startNum ;
            while (startNum >= finishNum) {
                hasil.push(startNum);
                startNum -= step ;
            }                  
        }
        return hasil;
    }           
}

console.log(rangeWithStep(11,23,3));

console.log('------------------');
console.log('');
console.log('Soal No. 3 (Sum of Range)');
console.log('------------------');

function sum(firstVar, secondVar, step){
    var tmpHasil = [];
    var tmp=0;
    if (arguments.length == 2){
        step = 1;
    }else if (arguments.length == 1){
        step = 1;
        secondVar = 0;
    }

    tmpHasil = rangeWithStep(firstVar, secondVar, step);
    for (var i=0;i<tmpHasil.length;i++){
        tmp += tmpHasil[i];
    }
    return tmp;
}

console.log(sum(20,10,2));

console.log('------------------');
console.log('');
console.log('Soal No. 4 (Array Multidimensi)');
console.log('------------------');

function dataHandling(input){
    for (var i=0;i<input.length;i++){
        console.log('Nomor ID : '+ input[i][0]);
        console.log('Nama Lengkap : '+ input[i][1]);
        console.log('Tempat Lahir : '+ input[i][2]);
        console.log('Tanggal Lahir : '+ input[i][3]);
        console.log('Hobi : '+ input[i][4]);
        console.log('');
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input);

console.log('------------------');
console.log('');
console.log('Soal No. 5 (Balik Kata)');
console.log('------------------');

function balikKata(kata){
    
    var tmp = '';
    var panjang = kata.length;
    while ( panjang > 0){
        panjang--;
        tmp += kata[panjang] ;
        
    }
    return tmp;
}
console.log(balikKata("SanberCode"));

console.log('------------------');
console.log('');
console.log('Soal No. 6 (Metode Array) - Opsi 2');
console.log('------------------');

var input1 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
/*
Catatan : 
     
   *array [tanggal,bulan,tahun] tidak bisa di Sort . Potensi tanggal > bulan, atau bulan > tanggal, bisa saja terjadi.
    Hanya untuk fulfill funsi join, penaggalan yg sudah di split, akan dimasukan kedalam variable
    arrTanggal yg isinya [tgl, bulan, tahun]. 
    Untuk format display akan menggunakan funsi join dd-mm-yyyy
   
*/
function dataHandling2(var1){
    
    var ttl,bulan,strBulan;
    let arrTanggal = [];
    let ttlArr = [];    
    let joinTanggal = [];
    
     
    var1.splice(1,1,'Roman Alamsyah Elsharawy');
    var1.splice(2,1,'Provinsi Bandar Lampung');    
    var1.splice(4,1,'Pria');
    var1.splice(5,1,'SMA Internasional Metro');
    ttl = var1[3];
   
    ttlArr= ttl.split('/');
    
    tgl = ttlArr[0];
    bulan = ttlArr[1];
    tahun = ttlArr[2];

    arrTanggal.push(tahun);
    arrTanggal.push(bulan);
    arrTanggal.push(tgl);
    
    joinTanggal = ttlArr.join('-');    
    
    switch (parseInt(bulan)){
        case 1 : {
                    strBulan = "Januari";
                    break;
                }
        case 2 : {
                    strBulan = "Februari";
                    break;
                }
        case 3 : {
                    strBulan = "Maret";
                    break;
                }
        case 4 : {
                    strBulan = "April";
                    break;
                }
        case 5 : {
                    strBulan = "Mei";
                    break;
                }
        case 6 : {
                    strBulan = "Juni";
                    break;
                }
    
        case 7 : {
                    strBulan = "Juli";
                    break;
                }
        case 8 : {
                    strBulan = "Agustus";
                    break;
                }
        case 9 : {
                    strBulan = "September";
                    break;
                }
        case 10 : {
                    strBulan = "Oktober";
                    break;
                }
        case 11 : {
                    strBulan = "November";
                    break;
                }
        case 12 : {
                    strBulan = "Desember";
                    break;
                }
    }
   
    
    console.log(var1);
    console.log(strBulan);        
    console.log(arrTanggal);    
     
    console.log(joinTanggal);    
    
    console.log(var1[1].slice(0,14));    
    
   
}

dataHandling2(input1);


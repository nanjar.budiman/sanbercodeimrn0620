console.log('Function No 1');
console.log('--------------------');

function teriak(){
    return "Halo Sanbers!" ;
}
console.log(teriak());
console.log('');

console.log('Function No 2');
console.log('--------------------');

function kalikan(a,b){
    return a*b;
}

var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali) ;
console.log('');

console.log('Function No 3');
console.log('--------------------');

function introduce(a,b,c,d){
    var x = "Nama saya " + a + ", umur saya " + b + " tahun, alamat saya di "+c+", dan saya punya hobby yaitu "+d+"!";
    return x;
}
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan) ;
function readBooksPromise (time, book) {
    console.log(`saya mulai membaca ${book.name}`)
    return new Promise( function (resolve, reject){
      setTimeout(function(){
        //let sisaWaktu = time - book.timeSpent
        if(time >= book.timeSpent ){
            time = time - book.timeSpent;
            console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${time}`)
            
            resolve(time)
        } else {
            console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
            reject(time)
        }
      }, book.timeSpent)
    })
  }
   
  module.exports = readBooksPromise
/*
Soal No. 1 (Array to Object)
*/
console.log ('Soal No. 1 (Array to Object)');
function arrayToObject(arr) {
     
    var person = {};
    var umur,j;
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    j=1;
   for (var i=0;i<arr.length;i++){
       
        //(arr[i][3] == '') ||
        if ( (arr[i][3] == '') || ( parseFloat(arr[i][3])  > parseFloat(thisYear)) ){            
            umur = 'Invalid birth year';
        }else{              
            umur = parseFloat(thisYear) - parseInt(arr[i][3]) ;
        }
         
            person.firstName = arr[i][0];
            person.lastName = arr[i][1];
            person.gender   = arr[i][2];
            person.age      = umur;      
        console.log(j +'. '+ arr[i][0] + ' '+ arr[i][1] + ' : '+ JSON.stringify(person) );
        j++;
   }
      
}

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) ;

console.log('');
console.log('---------------------------');
/*
Soal No. 2 (Shopping Time)
*/
console.log('Soal No. 2 (Shopping Time)');
function shoppingTime(memberId, money) {
    var uang_cukup = false ;
    var masih_cukup = false ;
    var Tx = {};
    var members = [{
            memberId : "324193hDew2"
        },
        {
            memberId : "1820RzKrnWn08"
        },
        {
            memberId : "82Ku8Ma742"
        },
        {
            memberId : "234JdhweRxa53"
        }
    ];
    var products = [{
            product : "Sepatu brand Stacattu",
            price : 1500000
        },
        {
            product : "Baju brand Zoro",
            price : 500000
        },
        {
            product : "Baju brand H&N",
            price : 250000
        },
        {
            product : "Sweater brand Uniklooh",
            price : 175000
        },
        {
            product : "Casing Handphone",
            price : 50000
        }
    ];

    if (arguments.length < 2){
        console.log( "Mohon maaf, toko X hanya berlaku untuk member saja");
    }else{
        var ketemu = false;
      
        for (var i=0;i<members.length;i++){
           
            if (memberId == members[i].memberId){
                ketemu = true;
                break;
            }
        }

        if (ketemu){
            var x = 0;
            var listPurchased = [];
            var tmpChange = money ;
            for (var j=0;j < products.length;j++){
                if (parseFloat(tmpChange) >= products[j].price){
                   uang_cukup = true;
                   masih_cukup = true;
                    listPurchased.push(products[j].product);                   
                    tmpChange = parseFloat(tmpChange) - parseFloat(products[j].price);
                    
                    x++;
                }else{
                    masih_cukup = false;
                }
            }

            if (uang_cukup){
                Tx.memberId = memberId ;
                Tx.money = money ;
                Tx.listPurchased = listPurchased ;
                Tx.changeMoney = tmpChange ;
    
                console.log(Tx);
            }else{
                console.log( "Mohon maaf, uang tidak cukup");
            }
            
        }else{
            console.log( "Mohon maaf, toko X hanya berlaku untuk member saja");
        }
    }
     
}
shoppingTime('234JdhweRxa53', '15000');

console.log('');
console.log('---------------------------');
/*
Soal No. 3 (Naik Angkot)
*/
console.log('Soal No. 3 (Naik Angkot)');

function naikAngkot(arrPenumpang) {
    var trip = {};
    var harga = 200;
    var penumpang, naikDari, tujuan, bayar;
    var indexDari,indexTujuan;
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    for (var i=0;i < arrPenumpang.length;i++){
        trip.naikDari = arrPenumpang[i][1];
        trip.tujuan = arrPenumpang[i][2];
        indexDari = rute.indexOf(trip.naikDari);
        indexTujuan = rute.indexOf(trip.tujuan);

        bayar = harga * (indexTujuan - indexDari);

        trip.penumpang = arrPenumpang[i][0];
        trip.bayar      = bayar;
        
        console.log('penumpang : '+ trip.penumpang + ', naik dari: '+trip.naikDari+', tujuan: '+ trip.tujuan + ', bayar : '+ trip.bayar);
    }
  }
  naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]);
/*
1. Animal Class 
Release 0
*/

console.log('Release 0')
class Animal{
    constructor (name, legs, cold_blooded){
        this.name = name,
        this.legs = 4,
        this.cold_blooded = false

    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log('-----------------------------------')
console.log('');

console.log('Release 1');

class Ape extends Animal{
    constructor (name, yell){
        super(name), 
        this.legs = 2
    }
    yell(){
        return "Auooo";
    }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.yell()) ;// "Auooo"

class Frog  extends Animal{
    constructor (name, jump){
        super(name)
    }
    jump(){
        return "hop hop";
    }
}

var kodok  = new Frog("buduk");
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.jump()) ;// "hop hop"

console.log('-----------------------------------');
console.log('');
/*
2. Function to Class
*/

class Clock{
    constructor ({template}, timer, hours, mins,secs){
        this.template = template; 
        this.date = new Date;
        this.hours = hours;
        this.mins   = mins;
        this.secs   = secs;
       
    }
    render() {
       var date = new Date();

       this.hours = date.getHours();
        if (this.hours < 10) this.hours = '0' + this.hours;
    
       this.mins = date.getMinutes();
        if (this.mins < 10) this.mins = '0' + this.mins;
    
       this.secs = date.getSeconds();
        if (this.secs < 10) this.secs = '0' + this.secs;

        this.output = this.template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);   
       
       console.log(this.output);
    }
    stop () {
        clearInterval(this.timer);
    }
    start () {
       
       this.render();
       this.timer = setInterval(this.render.bind(this), 1000);
        
      }
}

var clock = new Clock({template: 'h:m:s'});

clock.start();  
clock.stop();
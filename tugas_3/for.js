console.log('No. 2 Looping menggunakan for');
console.log('----------------------');

for (var i=1;i<=20;i++){
    if ( (i % 3) == 0){
        if ( (i % 2) == 1){
            console.log(i + ' - I love coding');
        }else{
            console.log(i + ' - Berkualitas');
        }
    }else{
        if ( (i % 2) == 1){
            console.log(i + ' - Santai');
        }else{
            console.log(i + ' - Berkualitas');
        }
    }
}

console.log('----------------------');
console.log('');
console.log('No. 3 Membuat Persegi Panjang');

var str = '';
for (var i=1;i<=4;i++){
    for (var j=1;j<8;j++){
        str += '#';
        
    }
    console.log(str);
    str = '';
}

console.log('----------------------');
console.log('');
console.log('No. 4 Membuat Tangga');
var str = '';
for (var i=1;i<=7;i++){
    for (var j=1;j<=i;j++){
        str += '#';
       
       
    }    
    console.log(str);
    str = '';
    
}

console.log('----------------------');
console.log('');
console.log('No. 5 Membuat Papan Catur');
var str = '';
var tmpstr = '';
for (var i=1;i<=8;i++){    
    for (var j=1;j<=8;j++){
        if ( (i % 2 == 1) ){            
            if ( (j % 2 == 1) ){
                tmpstr = ' ';
            }else{
                tmpstr = '#';
            }
        }else{
            if ( (j % 2 == 1) ){
                tmpstr = '#';
            }else{
                tmpstr = ' ';
            }
        }
        str = str + tmpstr;              
    }    
    console.log(str);
    str = '';
    
}
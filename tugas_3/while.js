console.log('While Pertama');
console.log('----------------------');
var counter = 1;
while (counter <= 20){
    if (counter % 2 == 0){
        console.log(counter + ' - I love coding');
    }
    counter++;
}
console.log('----------------------');
console.log('');
console.log('While Kedua');
var counter = 20;
while (counter > 1){
    if (counter % 2 == 0){
        console.log(counter + ' - I will become a mobile developer');
    }
    counter--;
}